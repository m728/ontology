# Ontology



## Vauge Issues


| Vauge | How it Should Be |
| ------ | ------ |
|  Scan Page      |  Page that allows user to scan the network on their own      |
|    User admin    |   Page that allows user to edit their own app settings    |

## Tasks

| **Category**    | **Details**                                                                                                                                                    |
|-----------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Objective**   | What is the Objective?                                                                                                          |
| **Results**     | What results do we want?                                                                                                                                       |
| **Roles**       | Who is doing what?                                                                                                                                            |
| **Rules**       | What minimum viable standards must be kept?                                                                                                                    |
| **Description** | Design a visually appealing homepage layout incorporating company branding elements and key navigation features. Must be mobile-responsive.                    |
| **Deliverables**| What are the deliverables and what format?.                                                                                                                          |
| **Resources**   | What resources (people, money, tools) are available and needed?                                         |
| **Evaluation**     | How will progress be evaluated.                                                                                                                  |
| **Notes**       | Prioritize clean design with intuitive navigation.                                                                                                             |
